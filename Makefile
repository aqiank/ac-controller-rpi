NAME=ac-controller
CC=gcc
CFLAGS=`pkg-config --cflags pocketsphinx libserialport` -g
LDFLAGS=`pkg-config --libs pocketsphinx libserialport`

all: ${NAME}

${NAME}: main.o
	${CC} -o ${NAME} main.o ${LDFLAGS}

${NAME}.o: main.c
	${CC} -c main.c ${CFLAGS}

clean:
	-@rm ${NAME} *.o 2> /dev/null || true
