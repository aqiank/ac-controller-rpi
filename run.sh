#!/bin/sh

cd /home/pi/Projects/ac-controller-rpi

./ac-controller \
-kws_threshold 1e-20 \
-adcdev "hw:1,0" \
-dict assets/ac-controller.dic \
-lm assets/ac-controller.lm \
-logfn /dev/null
