#include <signal.h>
#include <sphinxbase/err.h>
#include <sphinxbase/ad.h>
#include <pocketsphinx.h>

#include <libserialport.h>

enum Code {
    TOGGLE = 0xA,
    INFO = 0xA0,
    AIR_CON_OFF = 0xA1,
    AIR_CON_ON = 0xA2,
    DECREASE_TEMPERATURE = 0xA3,
    INCREASE_TEMPERATURE = 0xA4,
};

static struct sp_port *arduino_port;
static ps_decoder_t *ps;
static cmd_ln_t *config;
static int running;

static const arg_t cont_args_def[] = {
    POCKETSPHINX_OPTIONS,
    {"-adcdev",
     ARG_STRING,
     NULL,
     "Name of audio device to use for input."},
    CMDLN_EMPTY_OPTION
};

static void sleep_msec(int32 ms)
{
#if (defined(_WIN32) && !defined(GNUWINCE)) || defined(_WIN32_WCE)
    Sleep(ms);
#else
    /* ------------------- Unix ------------------ */
    struct timeval tmo;

    tmo.tv_sec = 0;
    tmo.tv_usec = ms * 1000;

    select(0, NULL, NULL, NULL, &tmo);
#endif
}

static void open_arduino_port()
{
    struct sp_port **head;
    struct sp_port **ports;
    enum sp_return ret;
    const char *arduino_port_name = NULL;

    ret = sp_list_ports(&head);
    if (ret != SP_OK) {
        fprintf(stderr, "Failed to list serial ports\n");
        exit(2);
    }

    for (ports = head; *ports != NULL; ports++) {
        const char *name = sp_get_port_name(*ports);
        const char *manufacturer = sp_get_port_usb_manufacturer(*ports);

        if (manufacturer && strstr(manufacturer, "Arduino")) {
            sp_copy_port(*ports, &arduino_port);

            ret = sp_open(arduino_port, SP_MODE_READ_WRITE);
            if (ret != SP_OK) {
                fprintf(stderr, "Failed to open Arduino port\n");
                exit(3);
            };

            arduino_port_name = name;
            break;
        }
    }

    if (arduino_port_name) {
        sp_set_baudrate(arduino_port, 9600);
        fprintf(stdout, "Successfully opened Arduino port at %s\n", arduino_port_name);
    }

    sp_free_port_list(head);
}

static void close_arduino_port()
{
    enum sp_return ret;

    if (!arduino_port) {
        return;
    }

    ret = sp_close(arduino_port);
    if (ret != SP_OK) {
        fprintf(stderr, "Failed to close Arduino port\n");
        return;
    }

    fprintf(stdout, "Succesfully closed Arduino port\n");
}

static void send_command(unsigned char command)
{
    static unsigned char buf[1] = { 0 };

    if (!arduino_port) {
        return;
    }

    buf[0] = command;
    sp_blocking_write(arduino_port, buf, 1, 1000);
    sp_drain(arduino_port);
}

static int speak(enum Code code)
{
    switch (code) {
    default:
    case TOGGLE:
        break;
    case INFO:
        break;
    case AIR_CON_OFF:
        system("aplay assets/air-con-off.wav");
        break;
    case AIR_CON_ON:
        system("aplay assets/air-con-on.wav");
        break;
    case DECREASE_TEMPERATURE:
        system("aplay assets/decreasing-temperature.wav");
        break;
    case INCREASE_TEMPERATURE:
        system("aplay assets/increasing-temperature.wav");
        break;
    }
}

static void read_arduino()
{
    char buf[4096] = { 0 };
    int nread;

    nread = sp_blocking_read(arduino_port, buf, sizeof(buf), 10);
    if (nread < 0) {
        char *err = sp_last_error_message();
        fprintf(stderr, "read_arduino(): %s\n", err);
        sp_free_error_message(err);
        return;
    }

    fprintf(stdout, "ARDUINO: %s\n", buf);
}

static void check_if_we_have_the_keyword(const char *detected_keywords)
{
    // Toggle
    if (strcmp(detected_keywords, "AIR CON") == 0) {
        send_command(TOGGLE);
        speak(TOGGLE);
        goto we_have_keyword;
    } else if (strcmp(detected_keywords, "AIR CON OFF") == 0) {
        send_command(AIR_CON_OFF);
        speak(AIR_CON_OFF);
        goto we_have_keyword;
    } else if (strcmp(detected_keywords, "AIR CON ON") == 0) {
        send_command(AIR_CON_ON);
        speak(AIR_CON_ON);
        goto we_have_keyword;
    } else if (strcmp(detected_keywords, "INCREASE TEMPERATURE") == 0) {
        send_command(INCREASE_TEMPERATURE);
        speak(INCREASE_TEMPERATURE);
        goto we_have_keyword;
    } else if (strcmp(detected_keywords, "DECREASE TEMPERATURE") == 0) {
        send_command(DECREASE_TEMPERATURE);
        speak(DECREASE_TEMPERATURE);
        goto we_have_keyword;
    } else if (strcmp(detected_keywords, "INFORMATION") == 0) {
        send_command(INFO);
        speak(INFO);
        goto we_have_keyword;
    }

    return;

we_have_keyword:
    read_arduino();
}

static void recognize_from_microphone()
{
    ad_rec_t *ad;
    int16 adbuf[2048];
    uint8 utt_started, in_speech;
    int32 k;
    char const *hyp;

    if ((ad = ad_open_dev(cmd_ln_str_r(config, "-adcdev"), (int) cmd_ln_float32_r(config, "-samprate"))) == NULL) {
        E_FATAL("Failed to open audio device\n");
    }

    if (ad_start_rec(ad) < 0) {
        E_FATAL("Failed to start recording\n");
    }

    if (ps_start_utt(ps) < 0) {
        E_FATAL("Failed to start utterance\n");
    }

    // We're running the whole program now
    running = 1;

    utt_started = FALSE;
    E_INFO("Ready....\n");

    while (running) {
        if ((k = ad_read(ad, adbuf, 2048)) < 0) {
            E_FATAL("Failed to read audio\n");
        }

        ps_process_raw(ps, adbuf, k, FALSE, FALSE);
        in_speech = ps_get_in_speech(ps);
        if (in_speech && !utt_started) {
            utt_started = TRUE;
            E_INFO("Listening...\n");
        }
        if (!in_speech && utt_started) {
            /* speech -> silence transition, time to start new utterance  */
            ps_end_utt(ps);
            hyp = ps_get_hyp(ps, NULL );
            if (hyp != NULL) {
                printf("%s\n", hyp);
                fflush(stdout);
                check_if_we_have_the_keyword(hyp);
            }

            if (ps_start_utt(ps) < 0) {
                E_FATAL("Failed to start utterance\n");
            }
            utt_started = FALSE;
            E_INFO("Ready....\n");
        }
        sleep_msec(10);
    }

    ad_close(ad);
}

static int start_keyword_detection()
{
    char const *cfg;

    ps_default_search_args(config);
    ps = ps_init(config);
    if (ps == NULL) {
        cmd_ln_free_r(config);
        return 1;
    }

    recognize_from_microphone();

    ps_free(ps);
    cmd_ln_free_r(config);

    return 0;
}

static void handle_signal(int sig)
{
    running = 0;
}

int main(int argc, char **argv)
{
    int ret;

    signal(SIGINT, handle_signal);
    signal(SIGTERM, handle_signal);

    open_arduino_port();

    config = cmd_ln_parse_r(NULL, cont_args_def, argc, argv, FALSE);
    ret = start_keyword_detection();

    close_arduino_port();

    return ret;
}
